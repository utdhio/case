# PINS
###################################
# 光センサ
LIGHT_CE0 = 0  # CE0(GPIO8番ピン)

# ギヤドモータ
MOTOR_OUTPUT_PIN_1 = 24
MOTOR_OUTPUT_PIN_2 = 25

# Lidar
LIDAR_RX = 23  # GPIO
LIDAR_BAUD_RATE = 115200
###################################
