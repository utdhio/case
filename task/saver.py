async def __saver():
    global save_dict
    if IS_SAVER:
        save_count = 0
        while True:
            save_time = time.time() - START_TIME
            save_data = [str(save_time)]
            if IS_CDS:
                save_data.append(cds.val)
            else:
                save_data.append(0)
            if IS_AIR:
                save_data += [str(air.temprature), str(air.relative_humidity),
                              str(air.pressure), str(air.altitude), str(air.altvel)]
            else:
                save_data += [0] * 5
            save_dict[save_time] = save_data

            if save_count >= 100:
                save_df = pd.DataFrame.from_dict(save_dict, orient="index")
                libcs_df_save(LOG_DATA_FILE_PATH, save_df)
                save_dict = {}
                save_count = 0

            save_count += 1
            await asyncio.sleep(0.001)
