async def task_rotate(seconds=3):
    logger.info("detach motor rotate start")
    with open(os.path.join(LOG_DATA_PATH, NOW + '_motor.csv'), "a") as f:
        f.write(str(time.time() - START_TIME) + "\n")
    await motor_detach.rotate(seconds, True)
    logger.info("detach motor rotated " + str(seconds) + " s")
    # await motor_detach.rotate(seconds / 2, True)
    #logger.info("detach motor rotated reverse " + str(seconds / 2) + " s")


async def task_adjust_motor(seconds=1):
    logger.info("adjust motor rotate start")
    await motor_detach.rotate(abs(seconds), seconds > 0)
    logger.info("detach motor rotated " + str(abs(seconds)) + " s")


async def task_detach_alt_thh(alt_thh):
    alt_current = air.altitude
    alt_release = alt_current - alt_thh
    logger.info("alt_current: " + str_f(alt_current))
    while air.altitude > alt_release:
        logger.info("alt: " + str_f(air.altitude) + " release: " + str_f(alt_release))
        await asyncio.sleep(0.1)
    await task_rotate(3)


async def task_run(alt=40, alt_thh=10, cds_thh=LIGHT_THH_DEFAULT, seconds=5):
    await asyncio.sleep(1)
    logger.info("start")
    alt_ground = air.altitude
    alt_max = alt_ground
    while True:
        if air.altitude > alt_max:
            alt_max = air.altitude
        # if air.altitude < alt_max - alt_thh and air.altitude - alt_ground < alt:
        #    if cds_thh == -1:
        #        break
        #    elif cds.val > cds_thh:
        #        break
        if cds.val > cds_thh:
            break
        logger.info(
            "alt_max: " +
            str_f(
                alt_max -
                alt_ground) +
            " alt: " +
            str_f(
                air.altitude -
                alt_ground) +
            " cds: " +
            str_f(
                cds.val))
        await asyncio.sleep(0.05)
    await asyncio.sleep(1)
    await task_rotate(seconds)
