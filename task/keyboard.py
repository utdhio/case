async def __keyboard():
    global IS_RUNNING
    global key
    _keyboard_is_init = True
    while True:
        key = await aioconsole.ainput()
        if key == "" and _keyboard_is_init:
            IS_RUNNING = True
            _keyboard_is_init = False
        if key == "q":
            IS_RUNNING = False
        await asyncio.sleep(0.1)
