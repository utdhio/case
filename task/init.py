logger.info("initialize start")

if not IS_SIM:
    if IS_AIR:
        air = LibcsAir(board.D7)
        logger.info("Air initialized")
    if IS_CAMERA:
        pass
        # camera = LibcsCamera()
        # logger.info("Camera initialized")
    if IS_CDS:
        cds = LibcsCds()
        logger.info("Cds initialized")
    if IS_MOTOR:
        motor_detach = LibcsMotor(MOTOR_OUTPUT_PIN_1, MOTOR_OUTPUT_PIN_2)
        logger.info("Motor initialized")
else:
    logger.warning("exec in the simulator")

save_dict_index = "time,cds,temprature,relative_humidity,pressure,altitude,altvel"
with open(LOG_DATA_FILE_PATH, 'a') as f:
    f.write(save_dict_index + "\n")

key = ""

logger.info("initialize finished")
