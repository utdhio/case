async def __init():
    if IS_CDS:
        cds.init(LIGHT_CE0)
    logger.info("init completed")


def __abort():
    global save_dict
    global main_coroutines
    global IS_RUNNING
    IS_RUNNING = False
    main_coroutines = []
    if IS_CDS:
        cds.abort()
        logger.info("cds aborted")
    if IS_SAVER:
        save_df = pd.DataFrame.from_dict(save_dict, orient="index")
        libcs_df_save(LOG_DATA_FILE_PATH, save_df)
        save_dict = {}
        logger.info("log saved")
    if IS_MOTOR:
        motor_detach.abort()
        logger.info("motor aborted")
    logger.info("abort completed")


def exec(_func):
    def wrapper(*args, **kwargs):
        async def func():
            global IS_RUNNING
            while not IS_RUNNING:
                await asyncio.sleep(0.1)
            await _func(*args, **kwargs)
            IS_RUNNING = False

        async def main():
            global main_coroutines
            main_coroutines = []
            main_coroutines += [__keyboard(), __saver(), __sensor_update()]
            main_coroutines += [func()]
            await __init()
            await asyncio.gather(
                *main_coroutines
            )
        aiorun.run(main())
        __abort()
    return wrapper
