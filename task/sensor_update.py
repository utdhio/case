async def __sensor_update():
    pre_time = time.time()
    while True:
        pre_time = time.time()
        if IS_AIR:
            air.update()
        if IS_CDS:
            cds.update()
        await asyncio.sleep(0.001)
