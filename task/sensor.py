async def sensor_show_air():
    while True:
        logger.info("temperature: " + str_f(air.temprature) +
                    " humidity: " + str_f(air.relative_humidity) +
                    " pressure: " + str_f(air.pressure) +
                    " altitude: " + str_f(air.altitude) +
                    " altvel: " + str_f(air.altvel))
        await asyncio.sleep(0.5)


async def sensor_show_lidar():
    while True:
        logger.info('distance: ' + str(lidar.distance) + ', strength: ' + str(lidar.strength))
        await asyncio.sleep(0.5)


async def sensor_show_cds():
    while True:
        logger.info("cds: " + str_f(cds.val))
        await asyncio.sleep(0.5)


async def sensor_show_sensor():
    while True:
        logger.info("cds: " + str_f(cds.val) +
                    " temperature: " + str_f(air.temprature) +
                    " humidity: " + str_f(air.relative_humidity) +
                    " pressure: " + str_f(air.pressure) +
                    " altitude: " + str_f(air.altitude) +
                    " altvel: " + str_f(air.altvel))
        await asyncio.sleep(0.5)


async def sensor_check_cds(LIGHT_THH=LIGHT_THH_DEFAULT):
    while True:
        bright_or_dark = "bright" if cds.val > LIGHT_THH else "dark"
        logger.info("cds: " + str_f(cds.val) + ", " + bright_or_dark)
        await asyncio.sleep(0.5)


async def sensor_check_release(test=True, bright_thh=3, descent_thh=3, LIGHT_THH=LIGHT_THH_DEFAULT):
    bright_count = 0
    descent_count = 0
    while True:
        if cds.val > LIGHT_THH:
            bright_count += 1
        else:
            bright_count = 0
        if air.altvel < 0:
            descent_count += 1
        else:
            descent_count = 0

        if bright_count >= bright_thh and descent_count >= descent_thh:
            logger.info(
                "cds: " + str_f(cds.val) + ", " + str(bright_count) + "/" + str(bright_thh) + ", " +
                "altvel: " + str_f(air.altvel) + ", " + str(descent_count) + "/" + str(descent_thh) +
                ", released")
            if not test:
                break
        else:
            logger.info(
                "cds: " + str_f(cds.val) + ", " + str(bright_count) + "/" + str(bright_thh) + ", " +
                "altvel: " + str_f(air.altvel) + ", " + str(descent_count) + "/" + str(descent_thh) +
                ", not yet")
        await asyncio.sleep(SENSOR_CYCLE)
