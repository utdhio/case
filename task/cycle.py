async def cycle_bt_check():
    await bt.server_connect()
    logger.info("bluetooth connected")
    while True:
        if not bt.is_connected(CASE_ADDR):
            logger.info("bluetooth is not connected")
        await asyncio.sleep(1)


async def cycle_bt_send():
    await bt.server_connect()
    logger.info("bluetooth connected")
    while True:
        await bt.send(str(bt_msg))
        await asyncio.sleep(SENSOR_CYCLE)
