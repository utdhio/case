from libcs.ipython_config import set_ipython_config

profile = "case"
exec_files = [
    "/params.py",
    "/task/keyboard.py",
    "/task/saver.py",
    "/task/sensor_update.py",
    "/task/cycle.py",
    "/task/init.py",
    "/task/common.py",
    "/task/sensor.py",
    "/task/task.py",
    "/shell/sensor.py",
    "/shell/task.py",
]

set_ipython_config(profile, exec_files)
