class Task:
    @exec
    async def rotate(self, seconds=3):
        await task_rotate(seconds)

    @exec
    async def adjust_motor(self, seconds=3):
        await task_adjust_motor(seconds)

    @exec
    async def detach_alt_thh(self, alt_thh=7):
        # alt_thh: 上からの高さ
        await task_detach_alt_thh(alt_thh)
        await asyncio.sleep(30)
        await wait_forever()

    @exec
    async def run(self, alt=40, alt_thh=10, cds_thh=LIGHT_THH_DEFAULT, seconds=5):
        await task_run(alt, alt_thh, cds_thh, seconds)
        await asyncio.sleep(30)
        await wait_forever()

    @exec
    async def run_simple(self, alt=40, alt_thh=10, seconds=5):
        await task_run(alt, -1, alt_thh, seconds)
        await asyncio.sleep(30)
        await wait_forever()


task = Task()
