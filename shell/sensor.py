class Sensor:
    @exec
    async def show_air(self):
        await sensor_show_air()

    @exec
    async def show_cds(self):
        await sensor_show_cds()

sensor = Sensor()
